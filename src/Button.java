import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;


public class Button extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ColorAction c1,c2,c3;
	
	
	public Button(){

		setSize(300, 300);
		setMinimumSize(new Dimension(300,300));
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton x1 = new JButton("green");
		JButton x2 = new JButton("blue");
		JButton x3 = new JButton("red");
		
		x1.setBounds(10, 10, 100, 40);
		x2.setBounds(10, 50, 100, 40);
		x3.setBounds(10, 90, 100, 40);
		
		add(x1);
		add(x2);
		add(x3);
		
		
		
		c1 = new ColorAction(Color.green,this);
		c2 = new ColorAction(Color.blue,this);
		c3 = new ColorAction(Color.red,this);
		
		x1.addActionListener(c1);
		x2.addActionListener(c2);
		x3.addActionListener(c3);
	
		
	}

	public static void main(String[] args){
		Button B = new Button();
		B.setVisible(true);
		
	}
}
