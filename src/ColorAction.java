import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColorAction implements ActionListener {

	Color bg;
	Button B;
	
	public ColorAction(Color x,Button B){
		bg = x;
		this.B = B;
	}
	public void actionPerformed(ActionEvent E) {
		
		B.setBackground(bg);
		System.out.println(bg);
	}
}
